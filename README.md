## TODO
- [x] HTTP server
- [x] Line notify API

## Dockerfile details
- Install fonts-thai-tlwg for using thai fonts (e.g. Laksaman)
- Install dependencies package for cairo <br>`sudo apt-get install build-essential libcairo2-dev libpango1.0-dev libjpeg-dev libgif-dev librsvg2-dev`
- Install node version 10
- Set timezone to Asia/Bangkok

## build dependencies
- python
- pkg-config
- libcairo2-dev
- make
- g++

## font
- fontconfig
- fonts-thai-tlwg