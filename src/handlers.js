const ChartjsNode = require("chartjs-node");
const Firestore = require("@google-cloud/firestore");
const request = require("superagent");

const { distributeColors } = require("./utils");
const { chartjsDefaultConfig } = require("./config");

const sendPM25ChartToLine = async () => {
  const db = new Firestore({
    projectId: "my-line-notify",
    // keyFilename: "my-line-notify-6f5dc7441884.json",
  });

  const places = new Map();

  const startDate = new Date(Date.now() - 24 * 60 * 60 * 1000);
  const snapshot = await db
    .collection("pm_2_5")
    .where("time", ">=", Firestore.Timestamp.fromDate(startDate))
    .orderBy("time")
    .get();

  snapshot.docs.forEach((doc) => {
    const place = doc.get("place_name");
    const data = {
      dateTime: doc.get("time").toDate(),
      value: doc.get("value"),
    };

    // Add to map
    if (places.has(place)) {
      const oldValues = places.get(place);
      if (!oldValues) {
        throw new Error(`panic: places should have this "${place}" key`);
      }
      places.set(place, [...oldValues, data]);
    } else {
      places.set(place, [data]);
    }
  });

  const colors = distributeColors(places.size);

  // Set dataset
  const chartjsConfig = JSON.parse(JSON.stringify(chartjsDefaultConfig));
  chartjsConfig.data = {
    datasets: [...places.entries()].map((place, index) => ({
      label: place[0],
      fill: colors[index],
      borderColor: colors[index],
      data: place[1].map((point) => ({
        x: point.dateTime,
        y: point.value,
      })),
    })),
  };

  // Create chart as image buffer 
  const chartNode = new ChartjsNode(640, 480);
  chartNode.on("beforeDraw", (Chartjs) => {
    Chartjs.defaults.global.defaultFontFamily = "Laksaman";
  });

  await chartNode.drawChart(chartjsConfig);
  const buffer = await chartNode.getImageBuffer("image/png");

  // send request
  const response = await request
    .post("https://notify-api.line.me/api/notify")
    .set("Authorization", `Bearer ${process.env.LINE_ACCESS_TOKEN}`)
    .attach("imageFile", buffer, {
      filename: "chart.png",
      contentType: "image/png",
    })
    .field("message", "PM2.5 Chart");

  console.log(response.status);
  console.log(response.header);
  console.log(response.body);
};

module.exports = {
    sendPM25ChartToLine
}