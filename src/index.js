const Koa = require("koa");
const Router = require("@koa/router");
const { sendPM25ChartToLine } = require("./handlers");

const router = new Router();
router.post("/chart", async (ctx) => {
  await sendPM25ChartToLine();
  ctx.body = "ok";
});

const app = new Koa();
app.use(router.routes());
app.listen(process.env.PORT, () =>
  console.log(`Server listening on port ${process.env.PORT}`)
);
