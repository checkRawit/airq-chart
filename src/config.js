// Chart config
module.exports = {
  chartjsDefaultConfig: {
    type: "line",
    data: {},
    options: {
      responsive: true,
      maintainAspectRatio: false,
      title: {
        display: true,
        text: "PM2.5 Time Point Data",
      },
      scales: {
        xAxes: [
          {
            type: "time",
            display: true,
            scaleLabel: {
              display: true,
              labelString: "Time",
            },
            time: { minUnit: "hour" },
            ticks: {
              major: {
                fontStyle: "bold",
                fontColor: "#FF0000",
              },
            },
          },
        ],
        yAxes: [
          {
            display: true,
            scaleLabel: {
              display: true,
              labelString: "PM2.5",
            },
          },
        ],
      },
    },
  },
};
