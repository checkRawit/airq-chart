module.exports = {
  distributeColors: (amount) => {
    const colors = [];
    for (var i = 0; i < amount; i++) {
      colors.push(`hsl(${Math.round((360 * i) / amount)}, 90%, 65%)`);
    }

    return colors;
  },
};
