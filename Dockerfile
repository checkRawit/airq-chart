FROM node:10-buster-slim

WORKDIR /app
RUN apt update && apt install python pkg-config libcairo2-dev make g++ fontconfig fonts-thai-tlwg -y --no-install-recommends
RUN ln -sf /usr/share/zoneinfo/Asia/Bangkok /etc/localtime
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile
COPY . ./

CMD ["yarn", "start"]